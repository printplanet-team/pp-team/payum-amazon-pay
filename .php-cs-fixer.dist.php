<?php

declare(strict_types=1);

$config = new PhpCsFixer\Config();

return $config
    ->setRules([
        '@PSR2' => true,
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
        'concat_space' => ['spacing' => 'one'],
        'ordered_imports' => ['sort_algorithm' => 'alpha'],
        'multiline_whitespace_before_semicolons' => ['strategy' => 'no_multi_line'],
        'no_unused_imports' => true,
        'not_operator_with_successor_space' => true,
        'phpdoc_no_empty_return' => false,
        'linebreak_after_opening_tag' => true,
        'blank_line_after_opening_tag' => true,
        'trim_array_spaces' => true,
        'no_superfluous_phpdoc_tags' => false,
    ])
    ->setUsingCache(false);
