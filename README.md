## Payum extension for Amazon Pay

Add to `composer.json`

```json
  {
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/printplanet-team/pp-team/payum-amazon-pay.git"
    }
  ]
}
```

Register a gateway factory to the payum's builder and create a gateway:

```php
<?php

use Amazon\Pay\API\Client;
use Payum\Core\PayumBuilder;
use Payum\Core\GatewayFactoryInterface;
use PrintPlanet\PayumAmazonPay\AmazonPayGatewayFactory;

$defaultConfig = [];

$payum = (new PayumBuilder)
    ->addGatewayFactory('amazon_pay', function(array $config, GatewayFactoryInterface $coreGatewayFactory) {
        return new AmazonPayGatewayFactory($config, $coreGatewayFactory);
    })

    ->addGateway('amazon_pay', [
        'factory' => 'amazon_pay',
        'payum.api' => new Client([
            'public_key_id' => 'xxxx',
            'private_key' => '/path/to/your/key',
            'region' => 'DE',
            'sandbox' => true,
        ]),
    ])

    ->getPayum()
;
```

