<?php

declare(strict_types=1);

namespace PrintPlanet\PayumAmazonPay\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHttpRequest;
use PrintPlanet\PayumAmazonPay\Request\ExecutePayment;
use PrintPlanet\PayumAmazonPay\Request\UpdatePayment;

class CaptureAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $this->gateway->execute($httpRequest = new GetHttpRequest());

        if (isset($httpRequest->query['cancelled'])) {
            $model['statusDetails']['state'] = 'Canceled';
            $model->replace($model);

            return;
        }

        if (isset($httpRequest->query['amazonCheckoutSessionId'])) {
            $executePayment = new ExecutePayment($request->getToken());
            $executePayment->setModel($model);
            $this->gateway->execute($executePayment);

            return;
        }

        $updatePayment = new UpdatePayment($request->getToken());
        $updatePayment->setModel($model);

        $this->gateway->execute($updatePayment);

        throw new HttpRedirect($model['webCheckoutDetails']['amazonPayRedirectUrl']);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
