<?php

declare(strict_types=1);

namespace PrintPlanet\PayumAmazonPay\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetStatusInterface;

class StatusAction implements ActionInterface
{
    /**
     * {@inheritDoc}
     *
     * @param $request GetStatusInterface
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        if (false === isset($model['statusDetails']['state'])) {
            $request->markUnknown();
            return;
        }

        if ('Open' === $model['statusDetails']['state']) {
            $request->markNew();

            return;
        }

        if ('Completed' === $model['statusDetails']['state']) {
            $request->markCaptured();

            return;
        }

        if ('Canceled' === $model['statusDetails']['state']) {
            $request->markCanceled();

            if ('Expired' === $model['statusDetails']['state']['reasonCode']) {
                $request->markExpired();
            }

            /* not an actual amazon pay reasonCode, but for when something internal went wrong */
            if ('Failed' === $model['statusDetails']['state']['reasonCode']) {
                $request->markFailed();
            }

            return;
        }

        $request->markUnknown();
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request): bool
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
