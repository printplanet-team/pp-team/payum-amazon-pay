<?php

declare(strict_types=1);

namespace PrintPlanet\PayumAmazonPay;

use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;
use PrintPlanet\PayumAmazonPay\Action\AuthorizeAction;
use PrintPlanet\PayumAmazonPay\Action\CancelAction;
use PrintPlanet\PayumAmazonPay\Action\CaptureAction;
use PrintPlanet\PayumAmazonPay\Action\ConvertPaymentAction;
use PrintPlanet\PayumAmazonPay\Action\NotifyAction;
use PrintPlanet\PayumAmazonPay\Action\RefundAction;
use PrintPlanet\PayumAmazonPay\Action\StatusAction;

class AmazonPayGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritdoc}
     */
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => 'amazon_pay',
            'payum.factory_title' => 'Amazon Pay',
        ]);

        $config->defaults([
            'payum.action.capture' => new CaptureAction(),
            'payum.action.authorize' => new AuthorizeAction(),
            'payum.action.refund' => new RefundAction(),
            'payum.action.cancel' => new CancelAction(),
            'payum.action.notify' => new NotifyAction(),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
        ]);

        if (! empty($config['payum.api']) && ! $config['payum.api'] instanceof ApiInterface) {
            throw new \LogicException('Payum Api should be an instance of ' . ApiInterface::class . '.');
        }
        if (empty($config['payum.api'])) {
            throw new \LogicException('Payum Api not should be empty.');
        }
    }
}
