<?php

declare(strict_types=1);

namespace PrintPlanet\PayumAmazonPay;

use Amazon\Pay\API\Client;

interface ApiInterface
{
    public function createSessionPayload(string $reviewPageUrl, string $storeId, array $allowedCountries): array;

    public function signSessionPayload(): string;

    public function getClient(): Client;
}
